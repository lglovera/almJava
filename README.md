## Examen Tecnico-Java

El examen cubre los requerimientos y los extras.

* Diseñar el modelado de clases y diagramas UML necesarios para documentar y comunicar el diseño.
* Debe existir una clase Dispatcher encargada de manejar las llamadas, y debe contener el método dispatchCall para que las asigne a los empleados disponibles.
    * Para cumplir con los puntos extra la clase Dispatcher, el metodo dispatchCall se encarga de asignar las llamadas por orden jerarquico a medida que se van realizando las llamadas, si no hay operadores disponibles las agrega a una lista de llamadas sin asignar.
* La clase Dispatcher debe tener la capacidad de poder procesar 10 llamadas al mismo tiempo (de modo concurrente).
    * Se distribuye las llamadas entre los operadores disponibles y estas llamadas se procesan en paralelo, para ello IncomingCall extiende de Thread.
* Cada llamada puede durar un tiempo aleatorio entre 5 y 10 segundos.
    * La clase IncomingCall utiliza un Random para determinar si la duracion de la llamada.
* Debe tener un test unitario donde lleguen 10 llamadas.
    * se instancia el dispatcher al principio de los tests usando @BeforeClass de forma tal que quede listo para todos los tests unitarios.

## extra
* Dar alguna solución sobre qué pasa con una llamada cuando no hay ningún empleado libre.
	* Se obtiene un Boolean aleatorio para determinar si la persona colgo la llamada o espera a ser atendido
* Dar alguna solución sobre qué pasa con una llamada cuando entran más de 10 llamadas concurrentes.
    * para cumplir con este punto se decidio utilizar una LinkedList para generar una cola ( FIFO ) de manera tal que a medida de que se liberen los operadores el primero en la fila sea atendido.
* Agregar los tests unitarios que se crean convenientes.
    * Se agrego un test donde se prueban 20 llamadas en simultaneo.
* Agregar documentación de código
    * Se agregaron comentarios formato JAVADOCS en cada uno de los metodos.
 