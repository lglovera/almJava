/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model;

import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;

/**
 *
 * @author llove
 */
public class IncomingCall extends Thread {
    private static final Logger LOG = Logger.getLogger(IncomingCall.class.getName());

    private int callId;
    private boolean hanged;
    private boolean callEnded;
    private Date startDate;
    private Date endDate;
    private Long time;
    private Employee assignedOperator;

    public IncomingCall() {
        callId = new Random().nextInt();
    }

    public int getCallId() {
        return callId;
    }

    public void setCallId(int callId) {
        this.callId = callId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Employee getAssignedOperator() {
        return assignedOperator;
    }

    public void setAssignedOperator(Employee assignedOperator) {
        this.assignedOperator = assignedOperator;
    }

    public boolean isHanged() {
        return hanged;
    }

    public void setHanged(boolean hanged) {
        this.hanged = hanged;
    }

    public boolean isCallEnded() {
        return callEnded;
    }

    public void setCallEnded(boolean callEnded) {
        this.callEnded = callEnded;
    }
    
    /**
    *   Genera un valor de tiempo aleatorio para la llamada, se llama a sleep para mantener la thread
    *   inactiva durante la duracion de la llamada, al finalizar se cargan los  datos se cargan al final
    */
    @Override
    public void run() {
        try {
            Random r = new Random();
            int min = 5000;
            int max = 10001;
            long timeAprox = r.nextInt(max - min) + min;

            sleep(timeAprox);
            
            this.setEndDate(new Date());
            this.setTime(timeAprox / 1000);
        } catch (InterruptedException e) {
            LOG.info("Llamada Interrumptida");
        } finally {
            this.setCallEnded(true);
            assignedOperator.endCurrentCall();
        }
    }
}
