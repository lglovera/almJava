/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model;

import com.almundo.service.Dispatcher;
import java.util.Date;

/**
 *
 * @author llove
 */
public class Employee {    
    private IncomingCall currentCall;
    private Dispatcher currentDispatcher;

    public Dispatcher getCurrentDispatcher() {
        return currentDispatcher;
    }

    public void setCurrentDispatcher(Dispatcher currentDispatcher) {
        this.currentDispatcher = currentDispatcher;
    }

    public IncomingCall getCurrentCall() {
        return currentCall;
    }
    
    public void startCall(IncomingCall call){
        this.currentCall = call;
        currentCall.setStartDate( new Date() );
        currentCall.setAssignedOperator(this);
        
        call.start();
    }
    
    public void endCurrentCall(){
        this.currentCall = null;
        
        this.getCurrentDispatcher().assignNextCall(this);
    }
}
