/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.service;

import com.almundo.model.Employee;
import com.almundo.model.IncomingCall;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author llove
 */
public class Dispatcher {

    private LinkedList<IncomingCall> unAssignedCalls;
    private ArrayList<Employee> operators;
    private ArrayList<Employee> supervisors;
    private Employee director;

    private Random r;
    

    public Dispatcher(ArrayList<Employee> operators, ArrayList<Employee> supervisors, Employee director) {
        this.operators = operators;
        this.supervisors = supervisors;
        this.director = director;
        this.unAssignedCalls = new LinkedList<>();
        r = new Random();
        
        operators.forEach((operator) -> {
            operator.setCurrentDispatcher(this);
        });
        
        supervisors.forEach((supervisor) -> {
            supervisor.setCurrentDispatcher(this);
        });
        
        director.setCurrentDispatcher(this);
    }

    /**
    *   Asigna la siguiente llamada 
    *
    * @param        employee  empleado del que se quiere actualizar el estado.
    */
    public synchronized void assignNextCall(Employee employee){
        if( unAssignedCalls.peek() != null ){
            if(employee.getCurrentCall() == null){
                employee.startCall(unAssignedCalls.poll());
            }
        }
    }
    
    /**
    *    Chequea que los integrantes de las diferentes jerarquias esten desocupados, si estan desocupados entonces
    *    se asignan llamadas deacuerdo al orden jerarquico, en caso de no haber operadores disponibles
    *    se utiliza un random para determinar si el cliente cuelga el telefono o espera a ser atendido.
    *
    * @param        call  llamada que debe ser despachada.
    */
    public void dispatchCall(IncomingCall call) {
        boolean available = false;

        for (Employee operator : operators) {
            if(operator.getCurrentCall() == null){
                operator.startCall(call);
                return;
            }            
        }
        
        for (Employee supervisor : supervisors) {
            if(supervisor.getCurrentCall() == null){
                supervisor.startCall(call);
                return;
            } 
        }
        
        if(director.getCurrentCall() == null && !available){
            if(director.getCurrentCall() == null){
                director.startCall(call);
                return;
            } 
        }
        
        if (r.nextBoolean()) {
            this.unAssignedCalls.add(call);
        }else{
            call.setHanged(true);
        }
    }

    public LinkedList<IncomingCall> getUnAssignedCalls() {
        return unAssignedCalls;
    }

    public void setUnAssignedCalls(LinkedList<IncomingCall> unAssignedCalls) {
        this.unAssignedCalls = unAssignedCalls;
    }

    public ArrayList<Employee> getOperators() {
        return operators;
    }

    public void setOperators(ArrayList<Employee> operators) {
        this.operators = operators;
    }

    public ArrayList<Employee> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(ArrayList<Employee> supervisors) {
        this.supervisors = supervisors;
    }

    public Employee getDirector() {
        return director;
    }

    public void setDirector(Employee director) {
        this.director = director;
    }
}
