/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.test;

import com.almundo.model.Employee;
import com.almundo.model.IncomingCall;
import com.almundo.service.Dispatcher;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author llove
 */
public class DispatcherTest {
    
    private static Dispatcher dispatcher; 

    private final static Logger log = Logger.getLogger(DispatcherTest.class.getName());
    
    public DispatcherTest() {
       
    }
    
    @BeforeClass
    public static void setUpClass() {
        ArrayList<Employee> operators = new ArrayList<>();
        
        for (int i = 0; i < 7; i++) {
            Employee emp = new Employee();
            operators.add(emp);
        }
        
        
        ArrayList<Employee> supervisor = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            Employee emp = new Employee();
            operators.add(emp);
        }
        
        Employee dir = new Employee();
        
        dispatcher = new Dispatcher(operators, supervisor, dir);
    }
    
    @AfterClass
    public static void tearDownClass() {

    }
   
     @Test
     public void test10Calls() {
        log.info("-----------------------------------------------------------------------------");
        log.info("------------------------- 10 llamadas ---------------------------------------");
        log.info("-----------------------------------------------------------------------------");
         
         ArrayList<IncomingCall> calls = new ArrayList<>();
         // se generan 10 llamadas.
         for (int i = 0; i < 10; i++) {
             IncomingCall call = new IncomingCall();
             call.setCallId(i);             
             calls.add(call);
         }
         // se cargan automaticamente al dispatcher
         for (IncomingCall call : calls) {
             dispatcher.dispatchCall(call);
         }         
         
        // se inicia un loop en el cual se verifica que las llamadas no duren mas de 10 segundos o menores de 5 segundos.
        try{
            boolean completed = false;
            while(!completed){
                Thread.sleep(2000);
                completed = true;

                log.info("-----------------------------------------------------------------------------");

                for (IncomingCall call : calls) {                    
                    boolean callEnded = checkCall(call);
                    
                    //Si la llamada no esta finalizada entonces se continua el loop hasta que todas las llamadas esten finalizadas
                    if( ( !callEnded ) && completed ){
                        completed = false;
                    }
                }
            }
        }catch(InterruptedException e){
           log.severe(e.getMessage()); 
        }
     }
     
    @Test
    public void test20Calls() {
        log.info("-----------------------------------------------------------------------------");
        log.info("------------------------- 20 llamadas ---------------------------------------");
        log.info("-----------------------------------------------------------------------------");

        
        ArrayList<IncomingCall> calls = new ArrayList<>();
         
         for (int i = 0; i < 20; i++) {
             IncomingCall call = new IncomingCall();
             call.setCallId(i);             
             calls.add(call);
         }
         
         for (IncomingCall call : calls) {
             dispatcher.dispatchCall(call);
         }
         
         boolean completed = false;
         try{
            while(!completed){
                Thread.sleep(2000);
                completed = true;

                log.info("-----------------------------------------------------------------------------");

                for (IncomingCall call : calls) {
                    boolean callEnded = checkCall(call);
                    
                    //Si la llamada no esta finalizada entonces se continua el loop hasta que todas las llamadas esten finalizadas
                    if( ( !callEnded ) && completed ){
                        completed = false;
                    }
                }

            }
         }catch(InterruptedException e){
            log.severe(e.getMessage()); 
         }
    }
    
    private boolean checkCall(IncomingCall call){
        Long time = call.getTime();
        // determina si la llamada esta en curso
        boolean assigned = call.getAssignedOperator() != null;

        log.info(call.getCallId() + " cancelada: " + call.isHanged() + " - asignada: " + assigned + " - tiempo: " + time);

        if(call.isCallEnded()){
            assertTrue(call.getTime() != null );
            assertTrue(call.getCallId() + " - el tiempo de la llamada no puede ser mayor a:" + time, call.getTime() <= 10  );
            assertTrue(call.getCallId() + " - el tiempo de la llamada no puede ser menor a:" + time, call.getTime() >= 5  );
        }
        
        return call.isHanged() || call.isCallEnded();
    }
}
